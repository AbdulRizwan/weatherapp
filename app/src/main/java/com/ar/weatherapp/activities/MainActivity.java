package com.ar.weatherapp.activities;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.ar.weatherapp.BroadcastLocation;
import com.ar.weatherapp.FontUtils;
import com.ar.weatherapp.R;
import com.ar.weatherapp.Utils;
import com.ar.weatherapp.api.model.WeatherModel;
import com.ar.weatherapp.model.MainScreenPresenterImpl;
import com.ar.weatherapp.presenter.MainScreenPresenter;
import com.ar.weatherapp.view.ActivityView;

import retrofit2.Response;

import static android.content.pm.PackageManager.PERMISSION_DENIED;
import static com.ar.weatherapp.ErrorCodeConstants.ACCESS_DISABLE;
import static com.ar.weatherapp.ErrorCodeConstants.LOCATION_ERROR;
import static com.ar.weatherapp.ErrorCodeConstants.LOCATION_EXCEPTION;
import static com.ar.weatherapp.ErrorCodeConstants.NULL_VALUE_ERROR;
import static com.ar.weatherapp.ErrorCodeConstants.SERVER_ERROR;


/**
 * An example full-screen activity
 */
public class MainActivity extends AppCompatActivity implements ActivityView, View.OnClickListener {

    private View mContentView, mErrorView;
    MainScreenPresenter presenter;
    ProgressBar progressBar;
    Button retry;
    BroadcastLocation broadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        setupListeners();
        setupRegister();
        passLocationToGetInfo();
    }

    private void setupRegister() {
        broadcast = new BroadcastLocation();
        broadcast.addListener(MainActivity.this);
        registerReceiver(broadcast, new IntentFilter("android.intent.weather.broadcast"));
    }

    @Override
    protected void onDestroy() {
        if (broadcast != null)
            unregisterReceiver(broadcast);
        super.onDestroy();

    }

    private void passLocationToGetInfo() {//now it will use gps/network
        presenter.startLocationService(this);
    }

    private void setupListeners() {
        retry.setOnClickListener(this);
    }

    private void init() {
        presenter = new MainScreenPresenterImpl(this);

        mContentView = findViewById(R.id.content_view);
        progressBar = findViewById(R.id.progress_bar);
        mErrorView = findViewById(R.id.error_view);
        retry = findViewById(R.id.buttonRetry);

        retry.setTypeface(FontUtils.getLight());
    }

    @Override
    public void setupLocationInformation(int code, String location) {
        presenter.doLocationInfoValidation(code, location);
    }

    @Override
    public void error(int type) {
        switch (type) {
            case LOCATION_ERROR:
                Utils.hideViews(progressBar);
                Utils.showViews(mErrorView);
                Snackbar.make(mContentView, R.string.location_error, Snackbar.LENGTH_LONG).show();
                break;
            case SERVER_ERROR:
                Utils.hideViews(progressBar);
                Utils.showViews(mErrorView);
                break;

            case PERMISSION_DENIED:
            case NULL_VALUE_ERROR:
            case LOCATION_EXCEPTION:

                Utils.showViews(mErrorView);
                Utils.hideViews(progressBar);
                Snackbar.make(mContentView, R.string.access_denied, Snackbar.LENGTH_INDEFINITE).show();
                break;

            case ACCESS_DISABLE:
                Utils.showViews(mErrorView);
                Utils.hideViews(progressBar);
                Snackbar.make(mContentView, R.string.enable_gps, Snackbar.LENGTH_INDEFINITE).show();
                break;

        }
    }

    @Override
    public void getServerResponse(Response<WeatherModel> response) {
        Log.d("mainActivity", "data getting: ");
        Utils.hideViews(progressBar, mErrorView);
        WeatherInfoActivity.newInstance(this, response.body());
    }

    @Override
    public void loadView(WeatherModel data) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonRetry:
                Utils.hideViews(mErrorView);
                Utils.showViews(progressBar);
                passLocationToGetInfo();
                break;
        }
    }
}
