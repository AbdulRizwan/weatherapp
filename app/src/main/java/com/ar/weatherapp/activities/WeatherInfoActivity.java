package com.ar.weatherapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ar.weatherapp.FontUtils;
import com.ar.weatherapp.R;
import com.ar.weatherapp.Utils;
import com.ar.weatherapp.adapters.WeatherListAdapter;
import com.ar.weatherapp.api.model.ForecastDay;
import com.ar.weatherapp.api.model.WeatherModel;
import com.ar.weatherapp.model.WeatherActivityPresImp;
import com.ar.weatherapp.presenter.WeatherActivityPresenter;
import com.ar.weatherapp.view.ActivityView;

import java.util.Collections;
import java.util.List;

import retrofit2.Response;

import static com.ar.weatherapp.ErrorCodeConstants.NULL_VALUE_ERROR;

/**
 * Created by abdul on 08/04/18.
 */

public class WeatherInfoActivity extends AppCompatActivity implements ActivityView {
    private WeatherActivityPresenter presenter;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private TextView subTitle;
    private static final String WEATHER_INFO = "weather_info";
    private WeatherListAdapter adapter;
    private List<ForecastDay> list;
    WeatherModel weatherInfo;
    View root;

    public static void newInstance(Context context, WeatherModel weatherInfo) {
        Intent intent = new Intent(context, WeatherInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(WEATHER_INFO, weatherInfo);
        ((Activity) context).overridePendingTransition(R.anim.sliding_bottom_to_up, R.anim.fade_anim_out);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather_info_layout);
        init();
        getIntentInfo();

        setupTitle();
        setupFont();
    }

    private void init() {
        presenter = new WeatherActivityPresImp(this);

        root = findViewById(R.id.rootLayout);
        recyclerView = findViewById(R.id.recyclerViewList);
        collapsingToolbarLayout = findViewById(R.id.collapsingToolbarLayout);
        subTitle = findViewById(R.id.subtitle);
    }

    private void setupFont() {
        collapsingToolbarLayout.setCollapsedTitleTypeface(FontUtils.getLight());
        subTitle.setTypeface(FontUtils.getThin());
    }

    private void setupTitle() {
        subTitle.setText(Utils.df.format(Math.ceil(weatherInfo.getCurrentTemp().getCurrentTemp())) + "\u00b0");
        collapsingToolbarLayout.setTitle(weatherInfo.getLocation().getName());
    }

    public void getIntentInfo() {
        weatherInfo = getIntent().getParcelableExtra(WEATHER_INFO);
        presenter.doValidation(weatherInfo);
    }


    @Override
    public void setupLocationInformation(int code, String location) {

    }

    @Override
    public void error(int type) {
        switch (type) {
            case NULL_VALUE_ERROR:
                Snackbar.make(root, R.string.null_error, Snackbar.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void getServerResponse(Response<WeatherModel> response) {

    }

    @Override
    public void loadView(WeatherModel info) {
        setupViewAdapter(info);
    }

    private void setupViewAdapter(WeatherModel info) {
        list = info.getForecast().getForecastDay();
        adapter = new WeatherListAdapter(this, list);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        Collections.sort(list);
        adapter.notifyDataSetChanged();

    }
}
