package com.ar.weatherapp;

/**
 * Created by abdul on 09/04/18.
 */

public interface ErrorCodeConstants {
    int LOCATION_ERROR = 1;
    int SERVER_ERROR = 2;
    int NULL_VALUE_ERROR = 3;
    int ACCESS_DISABLE = 4;
    int SUCCESS = 0;
    int LOCATION_EXCEPTION = 5;
}
