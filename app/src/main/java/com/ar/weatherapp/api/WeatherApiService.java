package com.ar.weatherapp.api;

import com.ar.weatherapp.api.model.WeatherModel;
import com.ar.weatherapp.constants.Constants;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * using for retrofit.
 * Created by abdul on 08/04/18.
 */

public interface WeatherApiService {

    @GET("forecast.json?key=" + Constants.KEY)
    Observable<Response<WeatherModel>> getWeatherInformation(@Query("q") String location, @Query("days") int days);
}
