package com.ar.weatherapp.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

/**
 * Created by abdul on 08/04/18.
 */

public class LocationTemp implements Parcelable {
    @Json(name = "name")
    String name;


    public String getName() {
        return name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    public LocationTemp() {
    }

    protected LocationTemp(Parcel in) {
        this.name = in.readString();
    }

    public static final Parcelable.Creator<LocationTemp> CREATOR = new Parcelable.Creator<LocationTemp>() {
        @Override
        public LocationTemp createFromParcel(Parcel source) {
            return new LocationTemp(source);
        }

        @Override
        public LocationTemp[] newArray(int size) {
            return new LocationTemp[size];
        }
    };
}
