package com.ar.weatherapp.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.squareup.moshi.Json;

/**
 * Created by abdul on 08/04/18.
 */

public class ForecastDay implements Parcelable, Comparable<ForecastDay> {
    @Json(name = "date_epoch")
    Long date;

    @Json(name = "day")
    Day day;

    public Long getDate() {
        return date;
    }

    public Day getDay() {
        return day;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.date);
        dest.writeParcelable(this.day, flags);
    }

    public ForecastDay() {
    }

    protected ForecastDay(Parcel in) {
        this.date = (Long) in.readValue(Long.class.getClassLoader());
        this.day = in.readParcelable(Day.class.getClassLoader());
    }

    public static final Parcelable.Creator<ForecastDay> CREATOR = new Parcelable.Creator<ForecastDay>() {
        @Override
        public ForecastDay createFromParcel(Parcel source) {
            return new ForecastDay(source);
        }

        @Override
        public ForecastDay[] newArray(int size) {
            return new ForecastDay[size];
        }
    };

    @Override
    public int compareTo(@NonNull ForecastDay day) {
        if (this.date <= day.getDate())
            return -1;
        else
            return 1;
    }
}
