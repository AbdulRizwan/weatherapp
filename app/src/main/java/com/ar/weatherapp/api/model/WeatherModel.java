package com.ar.weatherapp.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

/**
 * Created by abdul on 08/04/18.
 */

public class WeatherModel implements Parcelable {
    @Json(name = "forecast")
    ForecastModel forecast;

    @Json(name = "location")
    LocationTemp location;

    @Json(name = "current")
    Current currentTemp;

    public ForecastModel getForecast() {
        return forecast;
    }

    public LocationTemp getLocation() {
        return location;
    }

    public Current getCurrentTemp() {
        return currentTemp;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.forecast, flags);
        dest.writeParcelable(this.location, flags);
        dest.writeParcelable(this.currentTemp, flags);
    }

    public WeatherModel() {
    }

    protected WeatherModel(Parcel in) {
        this.forecast = in.readParcelable(ForecastModel.class.getClassLoader());
        this.location = in.readParcelable(LocationTemp.class.getClassLoader());
        this.currentTemp = in.readParcelable(Current.class.getClassLoader());
    }

    public static final Creator<WeatherModel> CREATOR = new Creator<WeatherModel>() {
        @Override
        public WeatherModel createFromParcel(Parcel source) {
            return new WeatherModel(source);
        }

        @Override
        public WeatherModel[] newArray(int size) {
            return new WeatherModel[size];
        }
    };
}
