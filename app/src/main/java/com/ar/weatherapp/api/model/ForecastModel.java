package com.ar.weatherapp.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdul on 08/04/18.
 */

public class ForecastModel implements Parcelable {
    @Json(name = "forecastday")
    List<ForecastDay> forecastDay;

    public List<ForecastDay> getForecastDay() {
        return forecastDay;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.forecastDay);
    }

    public ForecastModel() {
    }

    protected ForecastModel(Parcel in) {
        this.forecastDay = new ArrayList<ForecastDay>();
        in.readList(this.forecastDay, ForecastDay.class.getClassLoader());
    }

    public static final Parcelable.Creator<ForecastModel> CREATOR = new Parcelable.Creator<ForecastModel>() {
        @Override
        public ForecastModel createFromParcel(Parcel source) {
            return new ForecastModel(source);
        }

        @Override
        public ForecastModel[] newArray(int size) {
            return new ForecastModel[size];
        }
    };
}
