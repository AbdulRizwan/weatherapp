package com.ar.weatherapp.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

/**
 * Created by abdul on 08/04/18.
 */

public class Current implements Parcelable {
    @Json(name = "temp_c")
    double currentTemp;

    public double getCurrentTemp() {
        return currentTemp;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.currentTemp);
    }

    public Current() {
    }

    protected Current(Parcel in) {
        this.currentTemp = in.readDouble();
    }

    public static final Parcelable.Creator<Current> CREATOR = new Parcelable.Creator<Current>() {
        @Override
        public Current createFromParcel(Parcel source) {
            return new Current(source);
        }

        @Override
        public Current[] newArray(int size) {
            return new Current[size];
        }
    };
}
