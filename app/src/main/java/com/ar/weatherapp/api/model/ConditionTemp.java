package com.ar.weatherapp.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.squareup.moshi.Json;

/**
 * Created by abdul on 08/04/18.
 */

public class ConditionTemp implements Parcelable {

    @Json(name = "text")
    String conditionType;

    @Json(name = "icon")
    String icon;

    public String getConditionType() {
        return conditionType;
    }

    public String getIcon() {
        if (!TextUtils.isEmpty(icon))
            icon = "http://" + icon.substring(2, icon.length());
        return icon;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.conditionType);
        dest.writeString(this.icon);
    }

    public ConditionTemp() {
    }

    protected ConditionTemp(Parcel in) {
        this.conditionType = in.readString();
        this.icon = in.readString();
    }

    public static final Parcelable.Creator<ConditionTemp> CREATOR = new Parcelable.Creator<ConditionTemp>() {
        @Override
        public ConditionTemp createFromParcel(Parcel source) {
            return new ConditionTemp(source);
        }

        @Override
        public ConditionTemp[] newArray(int size) {
            return new ConditionTemp[size];
        }
    };
}
