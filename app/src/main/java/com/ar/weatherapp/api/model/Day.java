package com.ar.weatherapp.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

/**
 * Created by abdul on 08/04/18.
 */

public class Day implements Parcelable {
    @Json(name = "maxtemp_c")
    double maxTempC;

    @Json(name = "mintemp_c")
    double minTempC;

    @Json(name = "condition")
    ConditionTemp tempCondition;

    public double getMaxTempC() {
        return maxTempC;
    }

    public double getMinTempC() {
        return minTempC;
    }

    public ConditionTemp getTempCondition() {
        return tempCondition;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.maxTempC);
        dest.writeDouble(this.minTempC);
        dest.writeParcelable(this.tempCondition, flags);
    }

    public Day() {
    }

    protected Day(Parcel in) {
        this.maxTempC = in.readDouble();
        this.minTempC = in.readDouble();
        this.tempCondition = in.readParcelable(ConditionTemp.class.getClassLoader());
    }

    public static final Parcelable.Creator<Day> CREATOR = new Parcelable.Creator<Day>() {
        @Override
        public Day createFromParcel(Parcel source) {
            return new Day(source);
        }

        @Override
        public Day[] newArray(int size) {
            return new Day[size];
        }
    };
}
