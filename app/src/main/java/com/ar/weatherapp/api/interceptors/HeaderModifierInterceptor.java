package com.ar.weatherapp.api.interceptors;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by abdul on 08/04/18.
 */

public class HeaderModifierInterceptor implements Interceptor {
    String TAG = this.getClass().getName();

    public HeaderModifierInterceptor() {
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Request modifiedRequest = null;

        Log.d(TAG, " setup header modifier");
        modifiedRequest = originalRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .build();

        return chain.proceed(modifiedRequest);
    }
}

