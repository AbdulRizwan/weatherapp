package com.ar.weatherapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.weatherapp.FontUtils;
import com.ar.weatherapp.R;
import com.ar.weatherapp.Utils;
import com.ar.weatherapp.api.model.Day;
import com.ar.weatherapp.api.model.ForecastDay;

import java.util.List;

import static android.graphics.Typeface.BOLD;

/**
 * Created by abdul on 08/04/18.
 */

public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.MyViewHolder> {
    private static final String TAG = "WeatherListAdapter";
    private Context context;
    private List<ForecastDay> list;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date, temperature, condition;
        ImageView icon;

        public MyViewHolder(View view) {
            super(view);
            init(view);
            setupStyle();
        }


        private void setupStyle() {
            date.setTypeface(FontUtils.getThin(), BOLD);
            temperature.setTypeface(FontUtils.getThin());
            condition.setTypeface(FontUtils.getThin());
        }

        private void init(View view) {
            date = view.findViewById(R.id.date);
            temperature = view.findViewById(R.id.temperature);
            condition = view.findViewById(R.id.condition);
            icon = view.findViewById(R.id.icon);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public WeatherListAdapter(Context context, List<ForecastDay> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ForecastDay item = list.get(position);
        Day day = item.getDay();
        holder.temperature.setText(Utils.getRoundedValue(day.getMaxTempC()) + "\u00B0" + "/" + Utils.getRoundedValue(day.getMinTempC()) + "\u00B0");
        holder.condition.setText(Utils.getLastWord(day.getTempCondition().getConditionType()));
        holder.date.setText(Utils.getDatePrefix(item.getDate() * 1000));
        Utils.loadImage(context, holder.icon, day.getTempCondition().getIcon(), 0, 0);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
