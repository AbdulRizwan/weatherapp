package com.ar.weatherapp.view;

import com.ar.weatherapp.api.model.WeatherModel;

import retrofit2.Response;

/**
 * Created by abdul on 08/04/18.
 */

public interface ActivityView {
    void setupLocationInformation(int code, String location);

    void error(int type);

    void getServerResponse(Response<WeatherModel> response);

    void loadView(WeatherModel data);
}
