package com.ar.weatherapp;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import static android.content.pm.PackageManager.PERMISSION_DENIED;

/**
 * Created by abdul on 09/04/18.
 */

public class WeatherLocation extends IntentService {

    public static String CODE = "code";
    public static String LOCATION = "location";
    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    String BROADCAST_LOCATION = "android.intent.weather.broadcast";
    // Declaring a Location Manager
    protected LocationManager locationManager;

    public WeatherLocation() {
        super("com.ar.weather.location.service");
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                sendBroadcastInfo(ErrorCodeConstants.ACCESS_DISABLE, null);
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    sendBroadcastInfo(PERMISSION_DENIED, null);
                    return null;
                }
//
                Log.d("Network", "Network");
                if (locationManager != null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        sendBroadcastInfo(ErrorCodeConstants.SUCCESS, latitude + "," + longitude);
                        Log.d("data--->", latitude + " " + longitude);
                    }
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    Log.d("GPS Enabled", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            sendBroadcastInfo(ErrorCodeConstants.SUCCESS, latitude + "," + longitude);
                            Log.d("data--->", latitude + " " + longitude);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            sendBroadcastInfo(ErrorCodeConstants.LOCATION_EXCEPTION, null);
        }

        return location;
    }

    private void sendBroadcastInfo(int code, String location) {
        Intent resultIntent = new Intent(BROADCAST_LOCATION);
        resultIntent.putExtra(CODE, code);
        resultIntent.putExtra(LOCATION, location);
        sendBroadcast(resultIntent);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        getLocation();
    }

}