package com.ar.weatherapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.ar.weatherapp.view.ActivityView;

/**
 * Created by abdul on 09/04/18.
 */

public class BroadcastLocation extends BroadcastReceiver {
    ActivityView activityViewInterface;

    public void addListener(Activity activityView) {
        this.activityViewInterface = (ActivityView) activityView;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("onReceiver:", "" + intent.getStringExtra(WeatherLocation.LOCATION));
        activityViewInterface.setupLocationInformation(intent.getIntExtra(WeatherLocation.CODE, 1), intent.getStringExtra(WeatherLocation.LOCATION));
    }
}
