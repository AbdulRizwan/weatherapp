package com.ar.weatherapp.presenter;

import com.ar.weatherapp.api.model.WeatherModel;

/**
 * Created by abdul on 09/04/18.
 */

public interface WeatherActivityPresenter {
    void doValidation(WeatherModel data);

}
