package com.ar.weatherapp.presenter;

import android.content.Context;

/**
 * Created by abdul on 08/04/18.
 */

public interface MainScreenPresenter {
    void doLocationInfoValidation(int code, String location);

    void startLocationService(Context mContext);
}
