package com.ar.weatherapp;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Created by abdul on 08/04/18.
 */

public class Utils {
    public static DecimalFormat df = new DecimalFormat("##.#");
    private static String TAG = "Utils";
    public static final DateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");

    public static void hideViews(View... views) {

        for (View view : views) {
            if (view == null) continue;
            view.setVisibility(View.GONE);
        }
    }

    public static void showViews(View... views) {

        for (View view : views) {
            if (view == null) continue;
            view.setVisibility(View.VISIBLE);
        }
    }

    public static String getDatePrefix(long dateTime) {

        // Create a calendar object with today date. Calendar is in java.util pakage.
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        // Move calendar to yesterday
        calendar.add(Calendar.DATE, +1);

        // Get current date of calendar which point to the yesterday now
        calendar2.setTimeInMillis(dateTime);


        boolean isToday = calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
                calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) && calendar1.get(Calendar.DATE) == calendar2.get(Calendar.DATE);

        if (isToday)
            return "Today";
        else {
            boolean isYesterday = calendar.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
                    calendar.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) && calendar.get(Calendar.DATE) == calendar2.get(Calendar.DATE);
            if (isYesterday)
                return "Tomorrow";
            else
                return SIMPLE_DATE_FORMAT.format(dateTime);
        }
    }

    public static void loadImage(Context context, ImageView imageView, String url, int placeHolderUrl, int errorImageUrl) {
        Log.d(TAG, "" + url);
        Glide.with(context)
                .load(url)
                .placeholder(placeHolderUrl)
                .error(errorImageUrl)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

    }

    public static String getLastWord(String string) {
        if (!TextUtils.isEmpty(string))
            return string.substring(string.lastIndexOf(" ") + 1);
        else
            return "";
    }

    public static String getRoundedValue(double value) {
        return df.format(Math.ceil(value));
    }

    public static Spannable setStringWithColor(String mString, int colorId) {
        Spannable spannable = new SpannableString(mString);
        spannable.setSpan(new ForegroundColorSpan(colorId), 0, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        Log.d(TAG, spannable.toString());
        return spannable;
    }
}
