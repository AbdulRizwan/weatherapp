package com.ar.weatherapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by abdul on 09/04/18.
 */

public class WeatherApplication extends Application {

    private Context context;

    @Override
    public void onCreate() {
        context = this;
        FontUtils.initFonts(context);
        super.onCreate();
    }

}