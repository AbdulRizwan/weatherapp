package com.ar.weatherapp.model;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.ar.weatherapp.ErrorCodeConstants;
import com.ar.weatherapp.WeatherLocation;
import com.ar.weatherapp.api.RestClient;
import com.ar.weatherapp.api.model.WeatherModel;
import com.ar.weatherapp.presenter.MainScreenPresenter;
import com.ar.weatherapp.view.ActivityView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static android.content.pm.PackageManager.PERMISSION_DENIED;
import static com.ar.weatherapp.ErrorCodeConstants.SERVER_ERROR;

/**
 * Created by abdul on 08/04/18.
 */

public class MainScreenPresenterImpl implements MainScreenPresenter {
    ActivityView mainActivityView;

    public MainScreenPresenterImpl(ActivityView mainScreenView) {
        this.mainActivityView = mainScreenView;
    }

    @Override
    public void doLocationInfoValidation(int code, String location) {
        if (code == ErrorCodeConstants.SUCCESS) {
            doRestApiCall(location, 7);
        } else {
            Log.d("presenter", "error:" + code);
            mainActivityView.error(code);
        }
    }

    @Override
    public void startLocationService(Context mContext) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mainActivityView.error(PERMISSION_DENIED);
        } else {
            Intent intent = new Intent(mContext, WeatherLocation.class);
            mContext.startService(intent);
        }

    }

    private void doRestApiCall(String location, int days) {
        RestClient.getApiService().getWeatherInformation(location, days)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<WeatherModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("onSubscribe", "");
                    }

                    @Override
                    public void onNext(Response<WeatherModel> response) {
                        Log.d("onNext", response.code() + "");
                        if (response.code() == 200)
                            mainActivityView.getServerResponse(response);
                        else
                            mainActivityView.error(SERVER_ERROR);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("onError", e.getMessage());
                        mainActivityView.error(SERVER_ERROR);
                    }

                    @Override
                    public void onComplete() {
                        Log.d("onComplete", "");
                    }
                });
    }
}
