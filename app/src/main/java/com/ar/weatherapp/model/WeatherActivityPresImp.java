package com.ar.weatherapp.model;

import com.ar.weatherapp.ErrorCodeConstants;
import com.ar.weatherapp.api.model.WeatherModel;
import com.ar.weatherapp.presenter.WeatherActivityPresenter;
import com.ar.weatherapp.view.ActivityView;

/**
 * Created by abdul on 09/04/18.
 */

public class WeatherActivityPresImp implements WeatherActivityPresenter {
    ActivityView activityView;

    public WeatherActivityPresImp(ActivityView view) {
        activityView = view;
    }

    @Override
    public void doValidation(WeatherModel data) {
        if (data == null || data.getLocation() == null || data.getForecast() == null)
            activityView.error(ErrorCodeConstants.NULL_VALUE_ERROR);
        else
            activityView.loadView(data);
    }
}
