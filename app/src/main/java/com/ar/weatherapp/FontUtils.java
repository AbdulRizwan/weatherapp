package com.ar.weatherapp;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by abdul on 09/04/18.
 */

public class FontUtils {
    /***
     * * created a font static class to access form entire application based on the requirement.
     */
    public static Typeface bold, regular, medium, light, thin;

    public static void initFonts(Context context) {
        light = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        thin = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
    }

    public static Typeface getLight() {
        return light;
    }

    public static Typeface getThin() {
        return thin;
    }


}

